from django.db import models


class Proposal(models.Model):
    title = models.CharField(max_length=255, blank=True, default='')
    content = models.TextField(blank=True, default='')
