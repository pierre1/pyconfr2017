from django.views.generic import TemplateView

from rest_framework.viewsets import ModelViewSet

from .models import Proposal
from .serializers import ProposalSerializer


class Home(TemplateView):
    template_name = 'proposal/home.html'


class ProposalViewSet(ModelViewSet):
    queryset = Proposal.objects.all()
    serializer_class = ProposalSerializer
